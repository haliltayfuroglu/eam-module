module.exports =  function permit(...allowed) {
    const isAllowed = roles =>  roles.some(item => allowed.includes(item)); // allowed.indexOf(role) > -1;   
    // return a middleware
    return (req, res, next) => {
      console.log("************************");
      console.log(req.LoginUser);
      console.log(req.LoginUser.Roles.indexOf('Admin'))
      //args.forEach(arg => console.log(arg))
      if (req.LoginUser && isAllowed(req.LoginUser.Roles))
      {
        next(); // role is allowed, so continue on the next middleware
      }
      else {
        res.status(403).json({message: "Forbidden"}); // user is forbidden
      }
    }
  }