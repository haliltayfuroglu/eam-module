const db = require("../models");
const roleEnum = require("../helpers/enums/role.enum");
var bcrypt = require("bcryptjs");


const User = db.users;
const Role = db.roles;
const Program = db.programs;
const Event = db.events;
const Notification = db.notifications;
const UserNotification = db.userNotifications;

exports.seed = (req, res) => {
    return Promise.all([
        Role.create({ name: roleEnum.Admin }),
        Role.create({ name: roleEnum.Teacher }),
        Role.create({ name: roleEnum.Staff }),
        Role.create({ name: roleEnum.Student }),
        Role.create({ name: roleEnum.ProgramDirector }),
        Role.create({ name: roleEnum.ProgramManager }),
        Role.create({ name: roleEnum.User }),

        User.create({
            name: "Administrator", middlename: "", surname: "Administrator", mainRole: roleEnum.Admin,
            username: "admin", email: "admin@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Teacher1", middlename: "", surname: "Teacher1", mainRole: roleEnum.Teacher,
            username: "teacher1", email: "teacher1@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Staff1", middlename: "", surname: "Staff1", mainRole: roleEnum.Staff,
            username: "staff1", email: "staff1@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Student1", middlename: "", surname: "Student1", mainRole: roleEnum.Student,
            username: "student1", email: "student1@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "ProgramDirector", middlename: "", surname: "ProgramDirector", mainRole: roleEnum.ProgramDirector,
            username: "programDirector", email: "programDirector@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "ProgramDirector2", middlename: "", surname: "ProgramDirector2", mainRole: roleEnum.ProgramDirector,
            username: "programDirector2", email: "programDirector2@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "ProgramManager", middlename: "", surname: "ProgramManager", mainRole: roleEnum.ProgramManager,
            username: "programManager", email: "programManager@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "ProgramManager2", middlename: "", surname: "ProgramManager2", mainRole: roleEnum.ProgramManager,
            username: "programManager2", email: "programManager@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "ProgramManager3", middlename: "", surname: "ProgramManager3", mainRole: roleEnum.ProgramManager,
            username: "programManager2", email: "programManager2@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "User", middlename: "", surname: "User", mainRole: roleEnum.User,
            username: "user", email: "user@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Teacher2", middlename: "", surname: "Teacher2", mainRole: roleEnum.Teacher,
            username: "teacher2", email: "teacher2@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Teacher3", middlename: "", surname: "Teacher3", mainRole: roleEnum.Teacher,
            username: "teacher3", email: "teacher3@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Student2", middlename: "", surname: "Student2", mainRole: roleEnum.Student,
            username: "student2", email: "student2@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Student3", middlename: "", surname: "Student3", mainRole: roleEnum.Student,
            username: "student3", email: "student3@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Student4", middlename: "", surname: "Student4", mainRole: roleEnum.Student,
            username: "student4", email: "student4@mail", password: bcrypt.hashSync("12345", 8)
        }),
        User.create({
            name: "Student5", middlename: "", surname: "Student5", mainRole: roleEnum.Student,
            username: "student5", email: "student5@mail", password: bcrypt.hashSync("12345", 8)
        })

    ])
        .then(([adminRole, teacherRole, staffRole, studentRole, programDirectorRole, programManagerRole, userRole,
            adminUser, teacherUser, staffUser, studentUser, programDirectorUser, programDirectorUser2, programManagerUser, programManagerUser2, programManagerUser3,
            teacher2, teacher3, userUser, student2, student3, student4, student5]) => {

            Program.create({
                name: "Program 1", abbreviation: "PRG1", type: "Activity",
                schoolLevelRecommendation: "Elementary", expectedDuration: 43, startDate: "2018-03-29", "endDate": "2018-05-29",
                schoolYear: "3", programDirector: programDirectorUser.id, isActive: true
            }).then(
                program => {
                    program.setProgramManagers([programManagerUser, adminUser]),
                        program.setParticipants([studentUser, student2, student5])
                }
            );

            Program.create({
                name: "Program 2", abbreviation: "PRG2", type: "Study",
                schoolLevelRecommendation: "Middle", expectedDuration: 324, startDate: "2018-04-29", "endDate": "2018-09-29",
                schoolYear: "3", programDirector: programDirectorUser2.id, isActive: true
            }).then(
                program => {
                    program.setProgramManagers([programManagerUser2, programManagerUser3, teacher3]);
                    program.setParticipants([studentUser, student4, student3]);

                    Event.create({
                        program_id: program.id,
                        name: "Event 1",
                        description: "Event Description 1",
                        type: "Event Type 1",
                        venue: "Venue 1",
                        duration: 34,
                        eventDate: "2020-06-01",
                        isActive: true,
                        isApproved: true,
                        createdBy: adminUser.id,
                        updatedBy: adminUser.id
                    })
                        .then(event => {
                            Notification.create({ title: "Event Notification", description: event.name + " Added", isActive: true, createdBy: event.createdBy, updatedBy: event.updatedBy })
                                .then(notification => {
                                    Program.findAll(({
                                        include: [
                                            {
                                                model: User,
                                                as: 'Participants',
                                                through: { attributes: [] },
                                            }
                                        ],
                                        where: id = event.program_id
                                    }))
                                        .then(data => {
                                            notification.setNotifiedUsers(data[0].dataValues.Participants);
                                        })
                                });
                        });


                    Event.create({
                        program_id: program.id,
                        name: "Event 2",
                        description: "Event Description 2",
                        type: "Event Type 2",
                        venue: "Venue 2",
                        duration: 34,
                        eventDate: "2020-05-01",
                        isActive: true,
                        isApproved: true,
                        createdBy: adminUser.id,
                        updatedBy: adminUser.id
                    })
                        .then(event => {

                        });

                    Event.create({
                        program_id: program.id,
                        name: "Event 3",
                        description: "Event Description 3",
                        type: "Event Type 3",
                        venue: "Venue 3",
                        duration: 34,
                        eventDate: "2020-01-01",
                        isActive: true,
                        isApproved: true,
                        createdBy: adminUser.id,
                        updatedBy: adminUser.id
                    }).then(event => {

                    });

                    Event.create({
                        program_id: program.id,
                        name: "Event 4",
                        description: "Event Description 4",
                        type: "Event Type 4",
                        venue: "Venue 4",
                        duration: 34,
                        eventDate: "2019-01-01",
                        isActive: true,
                        isApproved: true,
                        createdBy: adminUser.id,
                        updatedBy: adminUser.id
                    }).then(event => {

                    });

                    Notification.create({ title: "Program Notification", description: program.name + " Added", isActive: true, createdBy: adminUser.id, updatedBy: adminUser.id })
                        .then(notification => {
                            Program.findAll(({
                                include: [
                                    {
                                        model: User,
                                        as: 'Participants',
                                        through: { attributes: [] },
                                    }
                                ],
                                where: id = program.id
                            }))
                                .then(data => {
                                    notification.setNotifiedUsers(data[0].dataValues.Participants);
                                })
                        });
                }
            );

            Notification.create({ title: "Program Notification", description: "New Program Added", isActive: true, createdBy: adminUser.id, updatedBy: adminUser.id })
                .then(notification => {
                    //UserNotification.create({ user_id: adminUser.id, notification_id: notification.id, isActive: true, isRead: false, createdBy: adminUser.id, updatedBy: adminUser.id });
                })

            return Promise.all([
                adminUser.setRoles([adminRole, teacherRole, staffRole, programDirectorRole, programManagerRole]),
                teacherUser.setRoles([teacherRole]),
                teacher2.setRoles([teacherRole]),
                teacher3.setRoles([teacherRole]),
                staffUser.setRoles([staffRole]),
                studentUser.setRoles([studentRole]),
                programDirectorUser.setRoles([programDirectorRole]),
                programManagerUser.setRoles([programManagerRole]),
                userUser.setRoles([userRole]),
                student2.setRoles([studentRole]),
                student3.setRoles([studentRole]),
                student4.setRoles([studentRole]),
                student5.setRoles([studentRole])
            ]);
        })
        .then()
        .catch(error => console.log(error));
};