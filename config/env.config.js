const dotenv = require('dotenv');
dotenv.config({ path: '../env/${process.env.NODE_ENV}.env' })
module.exports = {
  endpoint: process.env.API_URL,
  masterKey: process.env.API_KEY,
  port: process.env.PORT
};