const { host, port } = require('./env.config');
console.log(`Your port is ${port}`); // 8626

module.exports = {
    HOST: "localhost",
    USER: "eamBackendTestUser",
    PASSWORD: "eamBackendTestUser@069",
    DB: "eam_testdb",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };