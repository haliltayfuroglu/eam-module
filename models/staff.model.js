module.exports = (sequelize, Sequelize) => {
  const Staff = sequelize.define("staff", {
    user_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    department: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    } 
  },
  {
      freezeTableName: true
  });

Staff.associate = function (models) {
  Staff.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' })
};
return Staff;
};
