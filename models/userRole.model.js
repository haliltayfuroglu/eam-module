module.exports = (sequelize, Sequelize) => {
    const UserRole = sequelize.define("user_role_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        role_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'role',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    UserRole.associate = function (models) {
        UserRole.belongsTo(models.User, { foreignKey: 'user_id', targetKey: 'id', as: 'User' });
        UserRole.belongsTo(models.Role, { foreignKey: 'role_id', targetKey: 'id', as: 'Role' });
    };

    return UserRole;
};
