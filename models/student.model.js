module.exports = (sequelize, Sequelize) => {
  const Student = sequelize.define("student", {
    user_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    currentClass: {
      type: Sequelize.STRING(10),
      allowNull: false,
      defaultValue: true
    }
  },

    {
      freezeTableName: true
    });

  Student.associate = function (models) {
    Student.belongsTo(models.User, { foreignKey: 'user_id', as: 'User' })
  };
  return Student;
};
