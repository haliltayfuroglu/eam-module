module.exports = (sequelize, Sequelize) => {
  const Teacher = sequelize.define("teacher", {
    user_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    branch: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    }
  },
  {
      freezeTableName: true
  });

  Teacher.associate = function(models) {
    Teacher.belongsTo(models.User, {foreignKey: 'user_id', as: 'user'})
  };

  return Teacher;
};
