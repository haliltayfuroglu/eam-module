const role = require('../helpers/enums/role.enum');

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    middlename: {
      type: Sequelize.STRING,
      allowNull: true
    },
    surname: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    mainRole: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: role.Student
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    isActive: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    createdBy: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    updatedBy: {
      type: Sequelize.INTEGER,
      references: {
        model: 'user',
        key: 'id'
      }
    }
  },
    {
      freezeTableName: true
    });

  User.associate = function (models) {
    User.belongsToMany(models.Role, { as: 'Roles', through: models.UserRole, foreignKey: 'user_id' });
    User.belongsToMany(models.Program, { as: 'IncludedPrograms', through: models.programParticipant, foreignKey: 'user_id', otherKey: 'program_id' });
    User.belongsToMany(models.Program, { as: 'ManagedPrograms', through: models.programManager, foreignKey: 'user_id', otherKey: 'program_id' });

    User.belongsToMany(models.Notification, { as: 'Notifications', through: models.UserNotification, foreignKey: 'user_id', otherKey: 'notification_id' });


    User.hasMany(models.Event, { as: 'Events' });

     User.hasOne(models.Student, {as: 'StudentDetails', foreignKey: 'user_id', targetKey: 'id' });

  };

  // User.associate = function(models) {
  // };

  return User;
};
