module.exports = (sequelize, Sequelize) => {
    const Notification = sequelize.define("notification", {
        title: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        createdBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        updatedBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        }
    },
        {
            freezeTableName: true
        });

    Notification.associate = function (models) {
        Notification.belongsToMany(models.User, { as: 'NotifiedUsers', through: models.UserNotification, foreignKey: 'notification_id' });
    };

    return Notification;
};
