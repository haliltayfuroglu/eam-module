module.exports = (sequelize, Sequelize) => {
    const ProgramManager = sequelize.define("program_manager_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        program_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'program',
                key: 'id'
            }
        },
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    ProgramManager.associate = function (models) {
        ProgramManager.belongsTo(models.User, { foreignKey: 'user_id' });
        ProgramManager.belongsTo(models.Program, { foreignKey: 'program_id' });
    };
    return ProgramManager;
};
