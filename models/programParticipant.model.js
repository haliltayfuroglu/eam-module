module.exports = (sequelize, Sequelize) => {
    const ProgramParticipant = sequelize.define("program_participant_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        program_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'program',
                key: 'id'
            }
        },
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },        
        totalCompletion: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        totalAttendance: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    },
        {
            freezeTableName: true
        });

    ProgramParticipant.associate = function (models) {
        ProgramParticipant.belongsTo(models.User, { foreignKey: 'user_id' })
        ProgramParticipant.belongsTo(models.Program, { foreignKey: 'program_id' })
    };

    return ProgramParticipant;
};
