module.exports = (sequelize, Sequelize) => {
    const UserNotification = sequelize.define("user_notification_rel", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        notification_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'notification',
                key: 'id'
            }
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        isRead: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
    },
        {
            freezeTableName: true
        });

    UserNotification.associate = function (models) {
        UserNotification.belongsTo(models.User, { foreignKey: 'user_id', targetKey: 'id' })
        UserNotification.belongsTo(models.Notification, {foreignKey: 'notification_id', targetKey: 'id' })
    };

    return UserNotification;
};
