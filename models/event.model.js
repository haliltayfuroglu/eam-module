module.exports = (sequelize, Sequelize) => {
    const Event = sequelize.define("event", {       
        program_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'program',
                key: 'id'
            }
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },        
        eventDate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        venue: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: true
        },
        isApproved: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            allowNull : false,
            defaultValue : true
          },
        createdBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        },
        updatedBy: {
            type: Sequelize.INTEGER,
            references: {
                model: 'user',
                key: 'id'
            }
        }
    },
    {
        freezeTableName: true
    });

    Event.associate = function (models) {
        Event.belongsTo(models.Program, { foreignKey: 'program_id', as: 'Program' })
    };

    return Event;
};
