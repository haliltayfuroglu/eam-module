module.exports = (sequelize, Sequelize) => {
  const Role = sequelize.define("role", {   
    name: {
      type: Sequelize.STRING
    }
  },
  {
      freezeTableName: true
  }); 

  Role.associate = function(models) {
    // Role.belongsToMany(models.users, {through: 'user_role_rel', foreignKey: 'role_id', otherKey: 'user_id', as: 'roles'});

    Role.belongsToMany(models.User, { as: 'UsersInRole', through: models.UserRole, foreignKey: 'role_id'});

  };
  return Role;
};
