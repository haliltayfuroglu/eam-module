const dbConfig = require("../config/db.config.js");
const role = require("../helpers/enums/role.enum");

const Sequelize = require("sequelize");

var opts = {
  define: {
    //prevent sequelize from pluralizing table names
    freezeTableName: true
  }
}

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }, opts
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.roles = require("./role.model.js")(sequelize, Sequelize);
db.users = require("./user.model.js")(sequelize, Sequelize);
db.userroles = require("./userRole.model.js")(sequelize, Sequelize);

db.students = require("./student.model.js")(sequelize, Sequelize);
db.teachers = require("./teacher.model.js")(sequelize, Sequelize);
db.stafs = require("./staff.model.js")(sequelize, Sequelize);
db.programs = require("./program.model.js")(sequelize, Sequelize);
db.events = require("./event.model.js")(sequelize, Sequelize);
db.programManager = require("./programManager.model.js")(sequelize, Sequelize);
db.programParticipant = require("./programParticipant.model.js")(sequelize, Sequelize);

db.notifications = require("./notification.model.js")(sequelize, Sequelize);
db.userNotifications = require("./userNotification.model.js")(sequelize, Sequelize);


db.roles.belongsToMany(db.users, { as: 'UsersInRole', through: db.userroles, foreignKey: "role_id", targetKey: 'id', otherKey: "user_id" });
db.users.belongsToMany(db.roles, { as: 'Roles', through: db.userroles, foreignKey: "user_id", targetKey: 'id', otherKey: "role_id" });

db.programs.belongsToMany(db.users, { as: 'ProgramManagers', through: db.programManager, foreignKey: "program_id", targetKey: 'id', otherKey: "user_id" });
db.users.belongsToMany(db.programs, { as: 'ManagedPrograms', through: db.programManager, foreignKey: "user_id", targetKey: 'id', otherKey: "program_id" });

db.programs.belongsToMany(db.users, { as: 'Participants', through: db.programParticipant, foreignKey: "program_id", targetKey: 'id', otherKey: "user_id" });
db.users.belongsToMany(db.programs, { as: 'IncludedPrograms', through: db.programParticipant, foreignKey: "user_id", targetKey: 'id', otherKey: "program_id" });

db.notifications.belongsToMany(db.users, { as: 'NotifiedUsers', through: db.userNotifications, foreignKey: "notification_id", targetKey: 'id', otherKey: "user_id" });
db.users.belongsToMany(db.notifications, { as: 'Notifications', through: db.userNotifications, foreignKey: "user_id", targetKey: 'id', otherKey: "notification_id" });

db.programs.hasMany(db.events, { as: 'Events', foreignKey: 'program_id', sourceKey: 'id' })
db.events.belongsTo(db.programs, {as: 'Program',  foreignKey: 'program_id'  })


db.students.belongsTo(db.users, {foreignKey: 'user_id', targetKey: 'id'});
db.users.hasOne(db.students, {as: 'StudentDetails', foreignKey: 'user_id', targetKey: 'id'});

module.exports = db;
