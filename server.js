const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { authJwt } = require("./middlewares");
const permit = require("./middlewares/permission");
const error = require("./middlewares/error");


const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const { host, port } = require('./config/env.config');
const roleEnum = require('./helpers/enums/role.enum');
const seeder = require('./seeder/seed');
var bcrypt = require("bcryptjs");


const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./models");
const User = db.users;
const Role = db.roles;
const Notification = db.notifications;
const UserNotification = db.userNotifications;


db.sequelize.sync();
// // // //drop the table if it already exists
db.sequelize.sync({ force: true })
  .then(() => seeder.seed())
  .then(() => {
    console.log("Drop and re-sync db.");
  });

console.log(`Your port is ${process.env.PORT}`); // undefined
console.log(`Your port is ${port}`); // 8626


const swaggerDefinition = {
  info: {
    title: 'MySQL Registration Swagger API',
    version: '1.0.0',
    description: 'Endpoints to test the user registration routes',
  },
  host: 'localhost:3003',
  basePath: '/',
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'Authorization',
      scheme: 'bearer',
      in: 'header',
    },
  },
};
const options = {
  swaggerDefinition,
  apis: ['./routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});


console.log("Auth Route");
require('./routes/auth.routes')(app);
app.use(authJwt.verifyToken);

// routes
console.log("user Route");
require('./routes/user.routes')(app);

console.log("Student Route");
require('./routes/student.routes')(app);

console.log("program Route");
require('./routes/program.routes')(app);

console.log("Event Route");
require('./routes/event.routes')(app);

console.log("Notification Route");
require('./routes/notification.routes')(app);

console.log("User Notification Route");
require('./routes/userNotification.routes')(app);

app.use(error);

// set port, listen for requests
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});