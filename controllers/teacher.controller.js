const db = require("../models");
const databaseOperation = require("../database/db.js");
const roleEnum = require("../helpers/enums/role.enum");
var bcrypt = require("bcryptjs");


const Program = db.programs;
const Teacher = db.users;
const Teacher = db.teachers;
const Role = db.roles;
const Event = db.events;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

// Create and Save a new Teacher
exports.create = (req, res) => {
    // Validate request
    if (!req.body.username || !req.body.email || !req.body.password || !req.body.name
        || !req.body.surname || !req.body.branch) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Teacher
    const user = {
        name: req.body.name,
        middlename: req.body.middlename,
        surname: req.body.surname,
        mainRole: roleEnum.Teacher,
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        isActive: req.body.isActive ? req.body.isActive : true,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id,
    };

    // Save Teacher in the database
    Teacher.create(user)
        .then(user => {
            Teacher.create({ user_id: user.id, branch: req.body.branch })
                .then(teacher => {
                    Role.findAll({
                        where: {
                            name: {
                                [Op.eq]: roleEnum.Teacher
                            }
                        }
                    }).then(roles => {
                        user.setRoles(roles).then(() => {
                            res.send({ message: "Teacher was registered successfully!", data : teacher });
                        });
                    }).catch(err => {
                        res.status(500).send({ message: err.message });
                    });
                }
                ).catch(err => {
                    res.status(500).send({ message: err.message });
                });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }

    console.log(whereStatement);
    Teacher.findAll({
        where: whereStatement,
        include: [
            {
                model: Student,
                as: 'StudentDetails',
                attributes: ['currentClass']
            },
            {
                model: Role,
                as: 'Roles',
                through: { attributes: [] },
                where: { name: { [Op.eq]: roleEnum.Student } }
            },
            {
                model: Notification,
                as: 'Notifications',
                through: { attributes: [] }
            },
            {
                model: Program,
                as: 'IncludedPrograms',
                through: { attributes: [] }
            }
        ],
        attributes: {
            include: [],
            exclude: []
        }
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    console.log("Related Teacher " + relatedUserId);
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    Teacher.findAll(({
        include: {
            model: Teacher
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Teacher with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Teacher.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Teacher with id=" + id
            });
        });
};

// Update a Teacher by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.userId;
    Teacher.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Teacher was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Teacher with id=${id}. Maybe Teacher was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Teacher with id=" + id
            });
        });
};

// Delete a Teacher with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Teacher.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Teacher was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Teacher with id=${id}. Maybe Teacher was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Teacher with id=" + id
            });
        });
};

// Delete all Teachers from the database.
exports.deleteAll = (req, res) => {
    Teacher.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// find all pending Teacher
exports.assignParticipantsToProgram = (req, res) => {
    if (!req.body.Participants || req.body.Participants.length <= 0) {
        res.status(400).send({
            message: "Participants for student must be provided.!"
        });
        return;
    }

    const programId = req.params.id;
    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Teacher id must be provided" });
    }

    Teacher.findByPk(programId)
        .then(student => {
            let isApproved = this.isUserApprovedForProgram(req.LoginUser.id, student.id);

            if (isApproved) {
                let participantsForProgram = req.body.Participants;

                Teacher.findAll({
                    where: {
                        id: {
                            [Op.or]: participantsForProgram
                        }
                    }
                }).then(users => {
                    if (users && users.length > 0) {
                        console.log(users);
                        student.setParticipants(users).then(() => {
                            Notification.create({ title: "Added to Teacher", description: "You are added to '" + student.name + "'", isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
                                .then(notification => {
                                    notification.setNotifiedUsers(users);
                                    res.send({ message: "Users added for student '" + student.name + "' and notified." });
                                    return;
                                });
                        });
                    }
                    else {
                        res.status(400).send({
                            message: "Given users can not be found in system.."
                        });
                    }
                });
            }
            else {
                res.status(403).send({
                    message: "Teacher must have Admin, Teacher Director or Teacher Manager rights."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};


exports.findAllWithParticipants = (req, res) => {
    const programId = req.params.programId;
    const userId = req.params.userId;

    var whereStatementForProgram = {};
    var whereStatementForUser = {};
    var whereStatement = {};
    if (programId)
        whereStatementForProgram.id = { [Op.eq]: programId };
    if (userId)
        whereStatementForUser.id = { [Op.eq]: userId };
    Teacher.findAll(({
        include: [
            {
                model: Teacher,
                as: 'Participants',
                where: whereStatementForUser,
                through: { attributes: [] },

            }
        ],
        where: whereStatementForProgram
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};


exports.isUserApprovedForProgram = function (userId, programId) {

    var isAdminResult = true;


    databaseOperation.query("CALL CheckIsAdminForProgram(?,?)", [userId, programId], function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            var rows = JSON.parse(JSON.stringify(result[0]));
            callback(null, rows);
        }
    });

    function callback(err, rows) {
        console.log(rows);
        if (rows) {
            if (rows[0].length > 0) {
                console.log("results in callback:", rows);
                console.log("result length:", rows.length);
                isAdminResult = true;
            }
        }
    }

    return isAdminResult;
};

function getAdminUsersForProgram(userId, programId, callback) {
    sql.query("CALL CheckIsAdminForProgram(?,?)", [userId, programId], function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            var rows = JSON.parse(JSON.stringify(result[0]));
            callback(null, rows);
        }
    });
}
