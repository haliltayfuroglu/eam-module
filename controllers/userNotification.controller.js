const db = require("../models");
const Notification = db.notifications;
const User = db.users;
const Program = db.programs;
const UserNotification = db.userNotifications;
const Op = db.Sequelize.Op;

// Create and Save a new UserNotification
exports.create = (req, res) => {
    // Validate request
    if (!req.body.user_id || !req.body.notification_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a UserNotification
    const program = {
        user_id: req.body.user_id,
        notification_id: req.body.notification_id,
        isActive: req.body.isActive ? req.body.isActive : true,
        isRead: req.body.isRead ? req.body.isRead : false,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    // Save UserNotification in the database
    UserNotification.create(program)
        .then(program => {
            if (req.body.Participants) {
                User.findAll({
                    where: {
                        id: {
                            [Op.eq]: req.body.use
                        }
                    }
                }).then(users => {
                    program.setParticipants(users).then(() => {
                        res.send({ message: "UserNotification was registered successfully!" });
                    });
                });
            }

            res.send({ message: "UserNotification was registered successfully!" }, { program });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.user_id) {
        whereStatement.user_id = { user_id: { [Op.eq]: req.query.user_id } };
    }
    if (req.query.username) {
        whereStatement.notification_id = { notification_id: { [Op.eq]: req.query.notification_id } };
    }
    if (req.query.isActive) {
        whereStatement.isActive = { isActive: { [Op.eq]: isActive } };
    }
    if (req.query.isRead) {
        whereStatement.isRead = { isRead: { [Op.eq]: isRead } };
    }

    console.log(whereStatement);
    UserNotification.findAll({
        where: whereStatement
        // ,include :[
        //     {
        //         model: User
        //     }
        // ]
        
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    UserNotification.findAll(({
        include: {
            model: UserNotification
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single UserNotification with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    UserNotification.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving UserNotification with id=" + id
            });
        });
};

// Update a UserNotification by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.LoginUser.id;
    UserNotification.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            console.log("*************************hit : " +num);
            if (num == 1) {
                res.send({
                    message: "UserNotification was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update UserNotification with id=${id}. Maybe UserNotification was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating UserNotification with id=" + id +". Error details : "+err.message
            });
        });
};

// Delete a UserNotification with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    UserNotification.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "UserNotification was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete UserNotification with id=${id}. Maybe UserNotification was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete UserNotification with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    UserNotification.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// find all pending UserNotification
exports.findAllPending = (req, res) => {
    UserNotification.findAll({ where: { status: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving ezpenses."
            });
        });
};
