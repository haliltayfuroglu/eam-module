const db = require("../models");
const programConroller = require("../controllers/program.controller");
const roleEnum = require("../helpers/enums/role.enum");
const Program = db.programs;
const User = db.users;
const Event = db.events;
const Notification = db.notifications;

const Op = db.Sequelize.Op;

// Create and Save a new Event
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.description || !req.body.type || !req.body.eventDate ||
        !req.body.venue || !req.body.duration) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Event
    const event = {
        program_id: req.body.program_id,
        name: req.body.name,
        description: req.body.description,
        type: req.body.type,
        venue: req.body.venue,
        duration: req.body.duration,
        eventDate: req.body.eventDate,
        isActive: req.body.isActive ? req.body.isActive : true,
        isApproved: req.body.isActive ? req.body.isActive : false,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    if (req.LoginUser.mainRole === roleEnum.Student) {
        event.isActive = false;
    }

    Program.findByPk(event.program_id, {

        include: [
            {
                model: User,
                as: 'ProgramManagers',
                attributes: ['id', 'name', 'surname'],
                through: { attributes: [] }
            }
        ]
    })
        .then(program => {
            if (program) {
                // Save Event in the database
                Event.create(event)
                    .then(event => {
                        if (req.LoginUser.mainRole === roleEnum.Student) {
                            Notification.create({ title: "Event Added", description: "Event Added By User", isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
                                .then(notification => {
                                    console.log(program.ProgramManagers)
                                    notification.setNotifiedUsers(program.ProgramManagers);
                                });
                        }
                        res.send({ message: "Event was registered successfully!" }, { event });
                    })
                    .catch(err => {
                        res.status(500).send({ message: err.message });
                    });

            }
            else {
                res.status(400).send({ message: `There is not program which id is : ${event.program_id}` });
                return;
            }
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Events from the database.
exports.findAll = (req, res) => {

    console.log(req.LoginUser);
    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { name: { [Op.like]: '%' + req.query.name + '%' } };
    }
    if (req.query.username) {
        whereStatement.description = { description: { [Op.like]: `%${req.query.description}%` } };
    }
    if (req.query.email) {
        whereStatement.type = { type: { [Op.eq]: req.query.type } };
    }

    console.log(whereStatement);
    Event.findAll({
        where: whereStatement
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedProgramId = req.params.id;

    var whereStatement = {};
    if (relatedProgramId)
        whereStatement.id = { [Op.eq]: relatedProgramId };

    Event.findAll(({
        include: {
            model: Program
            , where: whereStatement
            , as: 'Program'
            , attributes: ['id', 'name']
        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Event with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Event.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Event with id=" + id
            });
        });
};

// Update a Event by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.userId;
    Event.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Event was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Event with id=${id}. Maybe Event was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Event with id=" + id
            });
        });
};

// Delete a Event with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Event.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Event was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Event with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Event.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Events were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForEvent = async (req, res) => {

    if (!req.body.title || !req.body.description) {
        res.status(400).send({
            message: "Notification Content can not be empty!"
        });
        return;
    }

    const eventId = req.params.id;
    var whereStatement = {};
    if (eventId)
        whereStatement.id = eventId;
    else {
        res.status(400).send({ messega: "Event id must be provided" });
    }

    Event.findByPk(eventId)
        .then(event => {

            let isApproved = programConroller.isUserApprovedForProgram(req.LoginUser.id, event.program_id);
            console.log(req.LoginUser.id + "******************* Is Approved :" + isApproved);

            if (isApproved) {
                let notificationTitle = req.body.title;
                let notificationDescription = req.body.description;

                Notification.create({ title: notificationTitle, description: notificationDescription, isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
                    .then(notification => {
                        Program.findAll(({
                            include: [
                                {
                                    model: User,
                                    as: 'Participants',
                                    through: { attributes: [] },
                                }
                            ],
                            where: id = event.program_id
                        }))
                            .then(data => {
                                notification.setNotifiedUsers(data[0].dataValues.Participants);
                                res.send({ notification });
                            })
                        res.send({ notification });
                    });
            }
            else {
                res.status(403).send({
                    message: "User must have Admin, Program Director or Program Manager rights."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};


// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForUpcomingEvents = async (req, res) => {

    if (!req.body.title || !req.body.description) {
        res.status(400).send({
            message: "Notification Content can not be empty!"
        });
        return;
    }

    const eventId = req.params.id;
    var whereStatement = {};
    if (eventId)
        whereStatement.id = eventId;
    else {
        res.status(400).send({ messega: "Event id must be provided" });
    }

    Event.findByPk(eventId)
        .then(event => {

            let isApproved = programConroller.isUserApprovedForProgram(req.LoginUser.id, event.program_id);
            console.log(req.LoginUser.id + "******************* Is Approved :" + isApproved);

            if (isApproved) {
                let notificationTitle = req.body.title;
                let notificationDescription = req.body.description;

                Notification.create({ title: notificationTitle, description: notificationDescription, isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
                    .then(notification => {
                        Program.findAll(({
                            include: [
                                {
                                    model: User,
                                    as: 'Participants',
                                    through: { attributes: [] },
                                }
                            ],
                            where: id = event.program_id
                        }))
                            .then(data => {
                                notification.setNotifiedUsers(data[0].dataValues.Participants);
                                res.send({ notification });
                            })
                        res.send({ notification });
                    });
            }
            else {
                res.status(403).send({
                    message: "User must have Admin, Program Director or Program Manager rights."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};