const db = require("../models");
const databaseOperation = require("../database/db.js");
const roleEnum = require("../helpers/enums/role.enum");
const Program = db.programs;
const User = db.users;
const Event = db.events;
const Notification = db.notifications;
const ProgramManager = db.programManager;

const Op = db.Sequelize.Op;

// Create and Save a new Program
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.abbreviation || !req.body.type || !req.body.schoolLevelRecommendation ||
        !req.body.expectedDuration || !req.body.startDate || !req.body.endDate || !req.body.schoolYear || !req.body.programDirector) {

        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Program
    const program = {
        name: req.body.name,
        abbreviation: req.body.abbreviation,
        type: req.body.type,
        schoolLevelRecommendation: req.body.schoolLevelRecommendation,
        expectedDuration: req.body.expectedDuration,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        schoolYear: req.body.schoolYear,
        programDirector: req.body.programDirector,
        isActive: req.body.isActive ? req.body.isActive : false,
        createdBy: req.LoginUser.id,
        updatedBy: req.LoginUser.id
    };

    // Save Program in the database
    Program.create(program)
        .then(program => {
            res.send({ message: "Program was added successfully!", data: program });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

// Retrieve all Programs from the database.
exports.findAll = (req, res) => {

    var whereStatement = {};
    if (req.query.name) {
        whereStatement.name = { [Op.like]: '%' + req.query.name + '%' };
    }
    if (req.query.username) {
        whereStatement.username = { username: { [Op.like]: `%${req.query.username}%` } };
    }
    if (req.query.email) {
        whereStatement.email = { email: { [Op.like]: `%${req.query.email}%` } };
    }

    console.log(whereStatement);
    Program.findAll({
        where: whereStatement,
        include: [
            {
                model: User,
                as: 'Participants',
                through: { attributes: [] }
            },
            {
                model: User,
                as: 'ProgramManagers',
                attributes: ['id', 'name', 'surname'],
                through: { attributes: [] }
            }
        ]
    })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findAllWithRelations = (req, res) => {
    const source = req.query.source;
    const relatedUserId = req.params.id;
    console.log("Related Program " + relatedUserId);
    var condition = source ? { source: { [Op.like]: `%${source}%` } } : null;

    var whereStatement = {};
    if (relatedUserId)
        whereStatement.id = relatedUserId;
    // if (searchParams.username)
    //   whereStatement.username = { $like: '%' + searchParams.username + '%' };


    Program.findAll(({
        include: {
            model: Program
            , where: whereStatement
            , as: 'CreatorUser'
            , attributes: ['id', 'name', 'middlename', 'surname']

        }
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findRelatedEvents = (req, res) => {
    const source = req.query.source;
    const programId = req.params.id;

    var whereStatement = {};
    console.log(programId + "**********************************")
    if (programId) {
        whereStatement.id = { [Op.eq]: programId }
    }
    // else {
    //     res.status(400).send({ messega: "Program id must be provided" });
    // }

    Program.findAll(({
        include: [{
            model: Event
            , where: {}
            , as: 'Events'
            //, attributes: []
            , required: false
        }],
        attributes: ['id', 'name']
        , where: whereStatement
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single Program with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Program.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Program with id=" + id
            });
        });
};

// Update a Program by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    req.body.updatedBy = req.userId;
    Program.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Program was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Program with id=${id}. Maybe Program was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Program with id=" + id
            });
        });
};

// Delete a Program with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Program.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Program was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Program with id=${id}. Maybe Program was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Program with id=" + id
            });
        });
};

// Delete all Programs from the database.
exports.deleteAll = (req, res) => {
    Program.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Programs were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};

// Assign Participants For Program
exports.assignParticipantsToProgram = (req, res) => {
    if (!req.body.Participants || req.body.Participants.length <= 0) {
        res.status(400).send({
            message: "Participants for program must be provided 'Participants : [ userId1, userId2, ... ]' format.!"
        });
        return;
    }

    const programId = req.params.id;
    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Program id must be provided" });
    }

    Program.findByPk(programId)
        .then(program => {
            let isApproved = this.isUserApprovedForProgram(req.LoginUser.id, program.id);

            if (isApproved) {
                let participantsForProgram = req.body.Participants;

                User.findAll({
                    where: {
                        id: {
                            [Op.or]: participantsForProgram
                        }
                    }
                }).then(users => {
                    if (users && users.length > 0) {
                        console.log(users);
                        program.setParticipants(users).then(() => {
                            Notification.create({ title: "Added to Program", description: "You are added to '" + program.name + "'", isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
                                .then(notification => {
                                    notification.setNotifiedUsers(users);
                                    res.send({ message: "Users added for program '" + program.name + "' and notified." });
                                    return;
                                });
                        });
                    }
                    else {
                        res.status(400).send({
                            message: "Given users can not be found in system.."
                        });
                    }
                });
            }
            else {
                res.status(403).send({
                    message: "User must have Admin, Program Director or Program Manager rights."
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while sending notifications for event."
            });
        });
};


exports.findAllWithParticipants = (req, res) => {
    const programId = req.params.programId;

    var whereStatementForProgram = {};
    var whereStatement = {};
    if (programId)
        whereStatementForProgram.id = { [Op.eq]: programId };

    Program.findAll(({
        include: [
            {
                model: User
                , as: 'Participants'
                , through: { attributes: [] }
                , required: false
            }
        ],
        where: whereStatementForProgram
        , attributes: ['id', 'name']
    }))
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};


// Send Notifications for Event to all user enrolled to program of event.
exports.sendNotificationsForUpcomingEvents = async (req, res) => {

    if (!req.body.title || !req.body.description) {
        res.status(400).send({
            message: "Notification Content can not be empty!"
        });
        return;
    }

    const programId = req.params.id;
    var whereStatement = {};
    if (programId)
        whereStatement.id = programId;
    else {
        res.status(400).send({ messega: "Event id must be provided" });
    }

//     Program.findByPk(programId)
//         .then(program => {
//             if (program) {
//                 let isApproved = programConroller.isUserApprovedForProgram(req.LoginUser.id, program.program_id);

//                 if (isApproved) {

//                     var whereStatement = {};
//                     whereStatement.eventDate = { [Op.gte]: Date.now() };

//                     Event.findAll(
//                         {
//                             where: whereStatement,
//                             include: [
//                                 {
//                                     model: Program,
//                                     as: 'Program',
//                                     attributes: ['id', 'name'],
//                                     include: [{
//                                         model: User
//                                         , as: 'Participants'
//                                         , through: { attributes: [] }
//                                         , attributes: ['id', 'name']
//                                         , where: { id: { [Op.eq]: studentId } }
//                                     }]
//                                 }
//                             ]
//                             , order: [['eventDate', 'asc']]
//                         }
//                             .then(data => {
//                                 res.send(data);
//                             })
//                             .catch(err => {
//                                 res.status(500).send({
//                                     message:
//                                         err.message || "Some error occurred while retrieving users."
//                                 });
//                             });

//                     let notificationTitle = req.body.title;
//                     let notificationDescription = req.body.description;

//                     Notification.create({ title: notificationTitle, description: notificationDescription, isActive: true, createdBy: req.LoginUser.id, updatedBy: req.LoginUser.id })
//                         .then(notification => {
//                             Program.findAll(({
//                                 include: [
//                                     {
//                                         model: User,
//                                         as: 'Participants',
//                                         through: { attributes: [] },
//                                     }
//                                 ],
//                                 where: id = program.program_id
//                             }))
//                                 .then(data => {
//                                     notification.setNotifiedUsers(data[0].dataValues.Participants);
//                                     res.send({ notification });
//                                 })
//                             res.send({ notification });
//                         });
//                 }
//                 else {
//                     res.status(403).send({
//                         message: "User must have Admin, Program Director or Program Manager rights."
//                     });
//                 }
//             }
//             else {
//                 res.status(404).send({
//                     message: "Program not found with id : " + programId
//                 });
//             }
//         })
//         .catch(err => {
//             res.status(500).send({
//                 message:
//                     err.message || "Some error occurred while sending notifications for event."
//             });
//         });
 };


exports.isUserApprovedForProgram = async (userId, programId) => {

    var isAdminResult = true;


    databaseOperation.query("CALL CheckIsAdminForProgram(?,?)", [userId, programId], function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            var rows = JSON.parse(JSON.stringify(result[0]));
            callback(null, rows);
        }
    });

    function callback(err, rows) {
        console.log(rows);
        if (rows) {
            if (rows[0].length > 0) {
                console.log("results in callback:", rows);
                console.log("result length:", rows.length);
                isAdminResult = true;
            }
        }
    }

    return isAdminResult;
};

function getAdminUsersForProgram(userId, programId, callback) {
    sql.query("CALL CheckIsAdminForProgram(?,?)", [userId, programId], function (err, result) {
        if (err) {
            callback(err, null);
        } else {
            var rows = JSON.parse(JSON.stringify(result[0]));
            callback(null, rows);
        }
    });
}
