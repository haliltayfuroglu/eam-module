const { authJwt } = require("../middlewares");
const controller = require("../controllers/student.controller");

module.exports = app => {

  var router = require("express").Router();

  router.get("/", controller.findAll);
  router.get("/:id/passedevents", controller.findPassedEvents);
  router.get("/:id/upcomingevents", controller.findUpcomingEvents);
  router.post("/", controller.create);

  app.use('/api/students', router);
};
