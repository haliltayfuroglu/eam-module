module.exports = app => {
    const events = require("../controllers/event.controller.js");
    const permit = require("../middlewares/permission");



    var router = require("express").Router();

    // Create a new Program
    router.post("/", events.create);

    // Retrieve all Programs
    router.get("/", permit("Admin"), events.findAll);
    router.get("/related/:id?", events.findAllWithRelations);

    // Retrieve all published Programs
    router.post("/:id/sendnotifications", events.sendNotificationsForEvent);

    // Retrieve a single Program with id
    router.get("/:id", events.findOne);

    // Update a Program with id
    router.put("/:id", events.update);

    // Delete a Program with id
    router.delete("/:id", events.delete);

    // Create a new Program
    router.delete("/", events.deleteAll);

    app.use('/api/events', router);
};
