module.exports = app => {
    const notifications = require("../controllers/notification.controller.js");


    var router = require("express").Router();

    // Create a new Program
    router.post("/", notifications.create);

    // Retrieve all Programs
    router.get("/", notifications.findAll);
    router.get("/related/:id?", notifications.findAllWithRelations);

    // Retrieve all published Programs
    router.get("/pending", notifications.findAllPending);

    // Retrieve a single Program with id
    router.get("/:id", notifications.findOne);

    // Update a Program with id
    router.put("/:id", notifications.update);

    // Delete a Program with id
    router.delete("/:id", notifications.delete);

    // Create a new Program
    router.delete("/", notifications.deleteAll);

    app.use('/api/notifications', router);
};
