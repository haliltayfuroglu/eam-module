module.exports = app => {
    const userNotifications = require("../controllers/userNotification.controller.js");


    var router = require("express").Router();

    // Create a new Program
    router.post("/", userNotifications.create);

    // Retrieve all Programs
    router.get("/", userNotifications.findAll);
    router.get("/related/:id?", userNotifications.findAllWithRelations);

    // Retrieve all published Programs
    router.get("/pending", userNotifications.findAllPending);

    // Retrieve a single Program with id
    router.get("/:id", userNotifications.findOne);

    // Update a Program with id
    router.put("/:id", userNotifications.update);

    // Delete a Program with id
    router.delete("/:id", userNotifications.delete);

    // Create a new Program
    router.delete("/", userNotifications.deleteAll);

    app.use('/api/usernotifications', router);
};
