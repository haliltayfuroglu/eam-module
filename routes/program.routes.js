module.exports = app => {
    const permit = require("../middlewares/permission");
    const usrRoles = require("../helpers/enums/role.enum");
    const programs = require("../controllers/program.controller.js");

    var router = require("express").Router();

    // Create a new Program
    router.post("/", permit(usrRoles.Admin), programs.create);


    ///GET
    // Retrieve all Programs
    router.get("/", programs.findAll);
    router.get("/related/:id?", programs.findAllWithRelations);
    router.get("/:programId?/participants", programs.findAllWithParticipants);

    //Get Programs with Related Events
    router.get("/:id?/relatedevents", programs.findRelatedEvents);

    // Retrieve a single Program with id
    router.get("/:id", programs.findOne);


    // Assign Participants for Program
    router.post("/:id/assigntoparticipants", permit(usrRoles.ProgramManager, usrRoles.ProgramDirector), programs.assignParticipantsToProgram);




    // Update a Program with id
    router.put("/:id", permit(usrRoles.Admin), programs.update);

    // Delete a Program with id
    router.delete("/:id", permit(usrRoles.Admin), programs.delete);

    // Create a new Program
    router.delete("/", permit(usrRoles.Admin), programs.deleteAll);

    app.use('/api/programs', router);
};
